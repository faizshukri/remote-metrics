# Solution 2

Using Prometheus to scrap metrics from remote server, and use Grafana to display the metrics.

## Prerequisites

- `docker` and `docker-compose`
- `virtualbox`
- `vagrant`

## Run

```
make
```

## Cleanup

```
make clean
```
