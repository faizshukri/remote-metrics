# Assessment

## Solution 1

Using Go as agent in the remote server (vagrant's virtualbox). Push available space to a message broker (RabbitMQ in host), and display the metric in PHP app also in host.

## Solution 2

Using Prometheus to scrap metrics from remote server, and use Grafana to display the metrics.
