<?php
require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;

function formatBytes($bytes, $precision = 2)
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    // Uncomment one of the following alternatives
    $bytes /= pow(1024, $pow);
    // $bytes /= (1 << (10 * $pow));

    return round($bytes, $precision) . ' ' . $units[$pow];
}

$connection = new AMQPStreamConnection($_ENV["MSG_BROKER_HOST"], 5672, 'user', 'bitnami');

$channel = $connection->channel();

$channel->queue_declare('hello', false, false, false, false);

$intro = "Available storages events.\n";
echo php_sapi_name() !== 'cli' ? nl2br($intro) : $intro;

$callback = function ($msg) {
    [$timestamp, $body] = explode(",", $msg->body);
    $message = " [" . date("Y-m-d H:i:s", $timestamp) . "] " . formatBytes($body) . "\n";
    echo php_sapi_name() !== 'cli' ? nl2br($message) : $message;
};

$channel->basic_consume('hello', '', false, true, false, false, $callback);

while ($channel->is_consuming()) {
    if (php_sapi_name() !== 'cli') {
        flush();
        ob_flush();
    }

    $channel->wait();
}
