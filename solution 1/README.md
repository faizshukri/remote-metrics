# Solution 1

Using Go as agent in remote server (in this case, vagrant's virtualbox) and RabbitMQ as the message broker.

## Prerequisites

- `make`
- `docker` and `docker-compose`
- `virtualbox`
- `vagrant`
- `go`

## Run

```
make
```

After everything run, wait for few seconds, and heads over to `http://localhost:8080`.

## Cleanup

```
make clean
```
