package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"syscall"

	"time"

	"github.com/streadway/amqp"
)

var rabbitmqUser = "user"
var rabbitmqPass = "bitnami"
var rabbitmqPort = 5672

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func getAvailableDiskSpace() string {
	var stat syscall.Statfs_t
	wd, err := os.Getwd()
	failOnError(err, "Failed to get working directory")
	syscall.Statfs(wd, &stat)
	return strconv.FormatUint(stat.Bavail*uint64(stat.Bsize), 10)
}

func main() {
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%d/", rabbitmqUser, rabbitmqPass, os.Getenv("MSG_BROKER_HOST"), rabbitmqPort))
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"hello", // name
		false,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

	for {
		body := fmt.Sprintf("%d,%s", time.Now().Unix(), getAvailableDiskSpace())
		err = ch.Publish(
			"",     // exchange
			q.Name, // routing key
			false,  // mandatory
			false,  // immediate
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        []byte(body),
			})
		failOnError(err, "Failed to publish a message")
		log.Println(body)

		time.Sleep(2 * time.Second)
	}
}
